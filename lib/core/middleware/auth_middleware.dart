import 'package:flutter/material.dart';
import 'package:flutter_middleware/main.dart';
import 'package:get/route_manager.dart';

class AuthMiddleware extends GetMiddleware {
  @override
  RouteSettings? redirect(String? route) {
    if (sharedPreferences!.getString("id") == null) {
      return const RouteSettings(name: '/signIn');
    } else {
      return null;
    }
  }
}
