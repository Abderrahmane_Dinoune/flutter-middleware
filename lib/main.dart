import 'package:flutter/material.dart';
import 'package:flutter_middleware/core/middleware/auth_middleware.dart';
import 'package:flutter_middleware/views/screens/home_screen.dart';
import 'package:flutter_middleware/views/screens/sign_in_screen.dart';
import 'package:get/route_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';

SharedPreferences? sharedPreferences;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  sharedPreferences = await SharedPreferences.getInstance();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Flutter middleware",
      initialRoute: '/',
      getPages: [
        GetPage(
            name: "/",
            page: () => const HomeScreen(),
            middlewares: [AuthMiddleware()]),
        GetPage(name: "/signIn", page: () => const SignInScreen()),
      ],
    );
  }
}
